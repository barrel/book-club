class CommentsRelations < ActiveRecord::Migration
  def change
    change_table(:comments) do |t|
      t.references :user, index: true, foreign_key: true
      t.remove :commenter
    end
  end
end
